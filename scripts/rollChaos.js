export async function rollChaos(chaosdice){
    var i;
    let chaossuccess = 0;
    let html = `<div>How Many Chaos Dice?<input id="chaos_box" value="${chaosdice}" /></div>`
    let chaos = await new Promise(resolve => {
        new Dialog({
            title: "Difficulty Modifiers",
            content: html,
            buttons: {
                ok: {
                    label: "OK",
                    callback: () => {
                        resolve({
                            "chaos": document.getElementById("chaos_box").value,
                        })
                    }
                }
            },
            default:"ok"
        }).render(true);
    });
    if (chaos.chaos){
        let r = new Roll(`${chaos.chaos}d6`);
        r.roll();
        if (game.dice3d){
            await game.dice3d.showForRoll(r);}
        let result = r.dice[0].results.map(r => r.result)
        let max = Math.max(...result)
        var numbersuccesses;
        if (max == 6){
            numbersuccesses = 1;
        }
        else {numbersuccesses = 0;}
        let chaossuccess = {"numbersuccess": numbersuccesses, "result": result}
        return chaossuccess;
    }

}