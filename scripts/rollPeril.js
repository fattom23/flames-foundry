import {getPeril} from './getPeril.js';

export async function rollPeril(actor){
    let peril = await getPeril(1);
        let perilNumber = peril.perillevel
        let rollValue = `${perilNumber}d10 + ${perilNumber}`;
        console.log(rollValue);
        let r = new Roll(`${rollValue}`)

        r.roll();
        if (game.dice3d){
            await game.dice3d.showForRoll(r);}
        console.log(r.result);
        ChatMessage.create({ content: `<div>${r.result}</div>` , speaker: ChatMessage.getSpeaker({alias: actor.name}) });
}
