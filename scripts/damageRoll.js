export async function damageRoll(inputs){
    let actor = inputs.actor;
    let bonus = inputs.damagebonus;
    console.log(bonus)
    const att = actor.data.data[bonus];
    console.log(att);

    let optionhtml = `<div>How Many Fury Dice?<input id="fury_box" type="number" value="1"/></div>
    <div>Any Static Bonus?<input id="static_box" type="number" value="0" /></div>`
    let fury = await new Promise(resolve => {
        new Dialog({
            title: "Fury Dice",
            content: optionhtml,
            buttons: {
                ok: {
                    label: "OK",
                    callback: () => {
                        resolve({
                            "fury": document.getElementById("fury_box").value,
                            "static": document.getElementById("static_box").value,
                        })
                    }
                }
            },
            default:"ok"
        }).render(true);
    });

    let rollNumber = `${fury.fury}d6x`;

    let r = await new Roll(rollNumber).roll();
    let results = r.dice[0].results.map(r => r.result)
    let min = Math.min(...results)


    if (game.dice3d){
        await game.dice3d.showForRoll(r);}

    let totalDamage = Number(att) + Number(r.total) + Number(fury.static);
    const html = `<div>You did <span title="${results}">${totalDamage}</span> damage!</div>
                    <div> Your bonus was ${att}</div>
                    <div>Your roll total was ${r.total}</div>
                    <div><button class="damage-reroll" data-actorid="${actor.id}" data-damage= ${totalDamage} data-lowDamage="${min}">Reroll the ${min} using Fortune</button></div>`
    ChatMessage.create({ content: html, speaker: ChatMessage.getSpeaker({alias: actor.name})      });


}
