export async function generateAttributes(actor){
    let r = new Roll('3d10', {async: true})
    await r.roll()
    let newcombat= Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newbrawn = Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newagility = Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newperception = Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newintelligence = Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newwillpower = Number(r.total) + Number(25);
    r = new Roll('3d10', {async: true})
    await r.roll()
    let newfellowship = Number(r.total) + Number(25);
    actor.update({"data.attributes.combat.value": newcombat, "data.attributes.intelligence.value": newintelligence, "data.attributes.willpower.value": newwillpower, "data.attributes.perception.value": newperception, "data.attributes.brawn.value": newbrawn, "data.attributes.agility.value": newagility, "data.attributes.fellowship.value": newfellowship} )

}