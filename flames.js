import flamesItemSheet from "./modules/sheets/item-sheet.js";
import PCsheet from "./modules/sheets/PCsheet.js";
import flamesActor from "./modules/actor.js";
import flamesTracker from "./modules/tracker.js";
import {flamesSkills} from "./modules/config.js";
import NPCsheet from "./modules/sheets/NPCsheet.js";
import {chatListeners} from "./scripts/chatListeners.js"

async function preloadHandlebarTemplates() {
    const templatepaths = [
        "systems/flames/templates/partials/skill-list.hbs",
        "systems/flames/templates/partials/skill-list-displayable.hbs",
        "systems/flames/templates/partials/profession-card.hbs",
        "systems/flames/templates/partials/attribute-list.hbs",
        "systems/flames/templates/partials/bonus-list.hbs",
        "systems/flames/templates/partials/item-card.hbs",
        "systems/flames/templates/partials/weapon-card.hbs",
        "systems/flames/templates/partials/condition-card.hbs",
        "systems/flames/templates/partials/description-card.hbs",
        "systems/flames/templates/partials/talent-card.hbs",
        "systems/flames/templates/partials/armor-card.hbs",
        "systems/flames/templates/partials/ancestry-card.hbs",
        "systems/flames/templates/partials/spell-card.hbs",
        "systems/flames/templates/partials/chaos-dots.hbs",
        "systems/flames/templates/partials/order-dots.hbs",
        "systems/flames/templates/partials/trapping-card.hbs",
    ];

    return loadTemplates(templatepaths);
}

Hooks.once('init', function () {
    console.log("Initializing Flames of Freedom");
    CONFIG.Actor.documentClass = flamesActor;
    CONFIG.flamesSkills = flamesSkills;
    // CONFIG.debug.hooks = true;
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("flames", PCsheet, {makeDefault: true});
    Actors.registerSheet("flames", NPCsheet, {makeDefault: true});
    Items.registerSheet("flames", flamesItemSheet, {makeDefault: true});

    game.settings.register('flames', 'gameName', {
      name: 'Name of Game',
      hint: 'Which Zweihander-based game are you playing?',
      scope: 'world',
      type: String,
      default: 'Zweihander',
      config: true,
      choices:{
        'Zweihander': 'Zweihander: Grim and Perilous',
        'Flames': 'Flames of Freedom'
      }
    }),

    game.settings.register('flames', 'chatOutput', {
      name: 'Alternate Chat Output?',
      hint: 'Use Alternate Chat Output Format?',
      scope: 'world',
      type: Boolean,
      default: false,
      config: true
    }),

    game.settings.register('flames', 'itemCreate', {
      name: 'Allow Item Creation on Character Sheets?',
      scope: 'world',
      type: Boolean,
      default: false,
      config: true
    }),

    game.settings.register('flames', 'misfortunePermissionLevel', {
        name: 'Misfortune Tracker User Role:',
        hint: 'Who should be allowed to amend the threat tracker? Please note, the permission level MUST have the Modify Configuration Settings permission.',
        scope: 'world',
        type: String,
        default: 'ASSISTANT',
        config: true,
        choices: {
          'PLAYER': 'Players',
          'TRUSTED': 'Trusted Players',
          'ASSISTANT': 'Assistant Gamemaster',
          'GAMEMASTER': 'Gamemasters',
        }
      });


    game.settings.register('flames', 'fortunePermissionLevel', {
        name: 'Fortune Tracker User Role:',
        hint: 'Who should be allowed to amend the fortune tracker? Please note, the permission level MUST have the Modify Configuration Settings permission.',
        scope: 'world',
        type: String,
        default: 'PLAYER',
        config: true,
        choices: {
          'PLAYER': 'Players',
          'TRUSTED': 'Trusted Players',
          'ASSISTANT': 'Assistant Gamemaster',
          'GAMEMASTER': 'Gamemasters',
        }
      });

      game.settings.register('flames', 'trackerRefreshRate', {
        name: 'Refresh Rate of Fortune & Misfortune:',
        hint: 'In seconds, how often should the tracker refresh. It is inadvisable to set this too low. Up this if it appears to be causing optimisation issues.',
        scope: 'world',
        type: Number,
        default: 5,
        config: true
      });

      game.settings.register('flames', 'misfortune', {
        scope: 'world',
        type: Number,
        default: 0,
        config: false
      });

      game.settings.register('flames', 'fortune', {
        scope: 'world',
        type: Number,
        default: 0,
        config: false
      });

      Handlebars.registerHelper("translate", function(name, options) {
        return flames.fn(name);
      });

      console.log(game);

    preloadHandlebarTemplates();

    Hooks.on('ready', function() {
        let error = false;
        let i = foundry.CONST.USER_ROLES[game.settings.get('flames', 'fortunePermissionLevel')];
        for (i; i <= 4; i++) {
          if (!game.permissions.SETTINGS_MODIFY.includes(i)) error = true;
        }
        if (error) {
          console.error('The Fortune Tracker User Role does not have permissions to Modify Configuration Settings. Please change one of these in Permission Configuration or System Settings.');
          ui.notifications.error('The Fortune Tracker User Role does not have permissions to Modify Configuration Settings. Please change one of these in Permission Configuration or System Settings.');
        }
        const t = new flamesTracker();
        renderTemplate('systems/flames/templates/sheets/tracker.hbs').then((html) => {
          t.render(true);
        });
      });
});

Hooks.on('preCreateItem', async (document, createData, options,) => {
  document.data.update({img:`systems/flames/assets/${document.type}.png`});
  return document;
})

Hooks.on('preCreateOwnedItem', async (document, createData, options) => {
  document.data.update({img:`systems/flames/assets/${document.type}.png`});
  return document;
})

Hooks.on('renderChatLog', (log, html, data) => {
  chatListeners(html);
});

Hooks.on('createItem',  async (item, options, userId) => {
  if (item.parent){
  const actor = item.parent;
    console.log("Hook Fired")
  if (item.type === 'Weapon' || item.type === 'Armor'){
  let list = item.data.qualities;
  let array = list.split(',').map(s=>s.trim())
  console.log(array);
  let pack = await game.packs.get("flames.zweihanderqualities");
  let index = await pack.getIndex();

  var s;
  var finalItem;
  for (s=0; s<array.length; s++){
  console.log(array[s]);
  let quality = index.find(i => i.name === array[s]);
  let duplicate = actor.items.filter(function(item) {return item.name === array[s]} );
  if (quality && duplicate.length < 1){
  let finalItem = await pack.getEntry(quality._id)

  
 await actor.createEmbeddedDocuments('Item', [finalItem])}
  }
  }
  }
})

Hooks.on('preCreateItem',  (item, createData, options) => {
  if (item.parent){
    let actor = item.parent;
  console.log("Precreate Hook Fired")
  const ancestry = actor.items.filter(function(item) {return (item.type == "Ancestry")} );
  if (ancestry.length > 0 && item.type == "Ancestry"){
    ui.notifications.error("There is already an ancestry");
    return false;
  }

  else if (item.type=="Profession"){
    const profession = actor.items.filter(function(item) {return (item.type == "Profession")} );
  const rptotal = (profession.length + 1) * 100;
  console.log(rptotal)
  console.log(item.data.data.rpspent)
    item.data.update({"data.rpspent": rptotal});
  }
}
})


// Hooks.on('updateItem', async (item, options, id) => {
//   item.img = `systems/flames/assets/${item.type}.png`;
//   return item;
// })
