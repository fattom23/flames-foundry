export default class flamesItemSheet extends ItemSheet{
    get template(){
        return `systems/flames/templates/sheets/${this.item.data.type}-sheet.hbs`;
        console.log(this);
    }

    static get defaultOptions() {
      return mergeObject(super.defaultOptions, {
        scrollY: [
          ".Advances .inventory-list",
          ".AdditionalInfo .inventory-list"
        ],
        tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "Advances"}]
      });
    }
    async getData() {
      const data = super.getData();
      const itemData = data.data;
      data.item = itemData;
      data.data = itemData.data;
      return data;
    }

    activateListeners(html) {
        super.activateListeners(html)


        html.find('.increase-skill').click(ev => {
            const item = this.item;
            const actor = this.actor;
            const skill = $(ev.currentTarget).attr("data-skill");
            const skillnumber = $(ev.currentTarget).attr("data-number");
            if (getProperty(item, `data.data.${skillnumber}.skillpurchased`) == false){
            let currentskill = getProperty(actor, `data.data.skills.${skill}`);
            let newskill = currentskill +1;
            let currentrp = getProperty(item, 'data.data.rpspent');
            let newrp = currentrp + 100;
            actor.update({[`data.skills.${skill}`]:  newskill});
            let bool = Boolean("true");
            item.update({[`data.${skillnumber}.skillpurchased`]: bool, ['data.rpspent']: newrp})
            }
            else{console.log("Advance already purchased")}
});

        html.find('.decrease-skill').click(ev => {
            const item = this.item;
            const actor = this.actor;
            const skill = $(ev.currentTarget).attr("data-skill");
            const skillnumber = $(ev.currentTarget).attr("data-number");
            if (getProperty(item, `data.data.${skillnumber}.skillpurchased`) == true){
            let currentskill = getProperty(actor, `data.data.skills.${skill}`);
            let newskill = currentskill -1;
            this.actor.update({[`data.skills.${skill}`]:  newskill});
            let currentrp = getProperty(item, 'data.data.rpspent');
            console.log(`Current RP is ${currentrp}`);
            let newrp = currentrp - 100;
            let bool = Boolean (false);
            item.update({[`data.${skillnumber}.skillpurchased`]: bool, ['data.rpspent']: newrp})
            }
            else{console.log("Skill Not Yet Purchased")}
    

});

html.find('.increase-bonus').click(ev => {
    const item = this.item;
    const actor = this.actor;
    const skill = $(ev.currentTarget).attr("data-skill");
    const skillnumber = $(ev.currentTarget).attr("data-number");
    if (getProperty(item, `data.data.${skillnumber}.bonuspurchased`) == false){
    let currentskill = getProperty(this.actor, `data.data.attributes.${skill}`);
    let newskill = currentskill +1;
    this.actor.update({[`data.attributes.${skill}`]:  newskill});
    let currentrp = getProperty(item, 'data.data.rpspent');
    let newrp = currentrp + 100;
    let bool = Boolean("true");
    item.update({[`data.${skillnumber}.bonuspurchased`]: bool, ['data.rpspent']: newrp})
    }
    else{console.log("Advance already purchased")}
});

html.find('.decrease-bonus').click(ev => {
    const item = this.item;
    const actor = this.actor;
    const skill = $(ev.currentTarget).attr("data-skill");
    const skillnumber = $(ev.currentTarget).attr("data-number");
    if (getProperty(item, `data.data.${skillnumber}.bonuspurchased`) == true){
    let currentbonus = getProperty(this.actor, `data.attributes.${skill}`);
    let newbonus = currentbonus -1;
    this.actor.update({[`data.attributes.${skill}`]:  newbonus});
    let currentrp = getProperty(item, 'data.data.rpspent');
    let newrp = currentrp - 100;
    let bool = Boolean (false);
    item.update({[`data.${skillnumber}.bonuspurchased`]: bool, ['data.rpspent']: newrp})
    }
    else{console.log("You have not yet purchased this advance")}
    

});

html.find('.increase-talent').click(async ev => {
    const item = this.item;
    const actor = this.actor;
    const skill = $(ev.currentTarget).attr("data-skill");
    const skillnumber = $(ev.currentTarget).attr("data-number");
    if (getProperty(item, `data.data.${skillnumber}.talentpurchased`) == false){
    let currentrp = getProperty(item, 'data.data.rpspent');
    let newrp = currentrp + 100;
    let bool = Boolean("true");
    item.update({[`data.${skillnumber}.talentpurchased`]: bool, ['data.rpspent']: newrp})
    let talentname = getProperty(item, `data.data.${skillnumber}.talentname`);
    let pack = await game.packs.get("flames.ZweihanderTalents");
    let index = await pack.getIndex();
    let talent = index.find(i => i.name === talentname);
    let duplicate = actor.items.filter(function(item) {return item.name === talentname} );
    if (talent && duplicate.length < 1){
    let finalItem = await pack.getDocument(talent._id)
    console.log(finalItem);
    await actor.createEmbeddedDocuments('Item', [finalItem.data])}
    else{ui.notifications.info('Talent Not in Compendium. Please Manually Add')}
    }
    else{ui.notifications.error("Advance already purchased")}
});

html.find('.delete-talent').click(async ev => {
    const item = this.item;
    console.log(item)
    const actor = this.actor;
    console.log(actor);
    const skill = $(ev.currentTarget).attr("data-skill");
    const skillnumber = $(ev.currentTarget).attr("data-number");
    if (getProperty(item, `data.data.${skillnumber}.talentpurchased`) == true){
    let currentrp = getProperty(item, 'data.data.rpspent');
    let newrp = currentrp - 100;
    let bool = Boolean (false);
    let talentname = getProperty(item, `data.data.${skillnumber}.talentname`);
    let exists = actor.data.items.find(i => i.name === talentname);
    if (exists){
    await actor.deleteEmbeddedDocuments('Item', [exists._id]);}
    item.update({[`data.${skillnumber}.talentpurchased`]: bool, ['data.rpspent']: newrp})
    }
    else{ui.notifications.error("You have not yet purchased this advance")}
    

});

html.find('.profession-delete').click(ev => {
    const item = this.item;
    const itemId = this.item._id;
    let skillnumber = "skill0";
    var i;
    for(i = 1; i<11; i++){
      skillnumber = `skill${i}`;
      if (item.data.data[skillnumber].skillpurchased == true){
        ui.notifications.error("Skills Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<8; i++){
      skillnumber = `bonus${i}`;
      if (item.data.data[skillnumber].bonuspurchased == true){
        ui.notifications.error("Bonus Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<4; i++){
      skillnumber = `talent${i}`;
      if (item.data.data[skillnumber].talentpurchased == true){
        ui.notifications.error("Talent Remaining to Remove");
        return;
      }
    }
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);
   
    // console.log(li);
    // console.log(`itemID =0 ${itemId}`);
    // this.actor.deleteEmbeddedEntity("OwnedItem", itemId);
  });




}

}
