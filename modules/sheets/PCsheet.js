import {skillRoll} from '/systems/flames/scripts/diceRoll.js';
import {damageRoll} from '/systems/flames/scripts/damageRoll.js';
import {rollChaos} from '/systems/flames/scripts/rollChaos.js';
import { rollOptions } from '../../scripts/rollOptions.js';
import {generateAttributes} from '../../scripts/generateAttributes.js';
import {flamesSkills} from '../config.js';
import {chaosChange} from '../../scripts/chaosChange.js';
import {orderChange} from '../../scripts/orderChange.js';
import {getPeril} from '../../scripts/getPeril.js';
import {rollPeril} from '../../scripts/rollPeril.js'

export default class PCSheet extends ActorSheet {
    get template() {
        return `systems/flames/templates/sheets/pc-sheet.hbs`;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
          scrollY: [
            ".Stats .inventory-list",
            ".Professions .inventory-list",
            ".Combat .inventory-list",
            ".Magick .inventory-list"
          ],
          tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description"}]
        });
      }


    async getData() {
        const data = super.getData();
        const itemData = data.data;
        data.item = itemData;
        data.data = itemData.data;
        data.professions = data.items.filter(function(item) {return item.type == "Profession"} );
        data.weapons = data.items.filter(function(item) {return item.type == "Weapon"} );
        data.armor = data.items.filter(function(item) {return item.type == "Armor"} );
        data.trappings = data.items.filter(function(item) {return item.type == "Trapping"} );
        data.conditions = data.items.filter(function(item) {return item.type == "Condition" && (item.data.conditiontype == "injury" || item.data.conditiontype == "condition") });
        data.talents = data.items.filter(function(item) {return item.type == "Talent"} );
        data.talentTraits = data.items.filter(function(item) {return item.type == "Talent" || (item.type == "Condition" && item.data.conditiontype == "trait")} );
        data.ancestries = data.items.filter(function(item) {return item.type == "Ancestry"} );
        data.spells = data.items.filter(function(item) {return item.type == "Spell"} );
        data.focuses = data.items.filter(function(item) {return item.type == "Focus"} );
        data.descriptions = data.items.filter(function(item) {return (item.type == "Profession" || item.type == "Talent" || item.type == "Condition" || item.type == "Disease") && item.data.conditiontype != "quality"})
        data.qualities = data.items.filter(function(item) {return item.type == "Condition" && item.data.conditiontype == "quality"} );
        data.drawbacks = data.items.filter(function(item) {return item.type == "Condition" && item.data.conditiontype == "drawback" });
        data.diseases = data.items.filter(function(item) {return item.type == "Condition" && item.data.conditiontype == "disease" });
        data.disorders = data.items.filter(function(item) {return item.type == "Condition" && item.data.conditiontype == "disorder" });
        data.mutations = data.items.filter(function(item) {return item.type == "Condition" && item.data.conditiontype == "mutation" });
        data.gameType = game.settings.get('flames', 'gameName');
        data.itemCreate = game.settings.get('flames', 'itemCreate');
        data.skillNames = CONFIG.flamesSkills.skillnames;
        data.conditionNames = await this.conditionNames();
        let groupName = "data.damage.value";
        let damagechoices = {0: "Unharmed", 1: "Lightly Wounded", 2: "<a class='roll-chaos-wounds' data-numdice='1' data-type='Moderate'> Moderately Wounded</a>", 3: "<a class='roll-chaos-wounds' data-numdice='2' data-type='Serious'>Seriously Wounded</a>", 4: "<a class='roll-chaos-wounds' data-numdice='3' data-type='Grievous'>Grievously Wounded</a>", 5: "Slain!"};
        data.damagechoices = damagechoices;
        let perilgroupName = "data.peril.value";
        let perilchoices = {0: "Unharmed", 1: "Imperiled", 2: "Ignore 1 Skill Rank", 3: "Ignore 2 Skill Ranks", 4: "Ignore 3 Skill Ranks", 5: "Incapacitated!"};
        data.perilchoices = perilchoices;
        return data;

    }

    async conditionNames(){
      let pack = await game.packs.get("flames.zweihanderconditions");
      let index = await pack.getIndex();
      return index;

    }

    activateListeners(html) {
        super.activateListeners(html)

        html.find(".item-create").click(this._onItemCreate.bind(this));

        html.find('.skill-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const type = $(ev.currentTarget).attr("data-type");
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type};
            skillRoll(inputs);
        });

        html.find('.attack-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const li = $(ev.currentTarget).parents(".item-name");
            let itemID = li.data("itemId");
            console.log(this.actor)
            const item = this.actor.data.items.get(itemID);
            let type = "advanced";
            if (skill == "simplemelee" || skill == "simpleranged"){
              type = "basic";
            }
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type, "attack": true, "weaponid": item.id };
            console.log(inputs);
            skillRoll(inputs);
        });
        html.find('.spell-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const li = $(ev.currentTarget).parents(".item-name");
            let itemID = li.data("itemId");
            const item = this.actor.items.get(itemId);
            const details = $(ev.currentTarget).attr("data-details");
            const critsucc = $(ev.currentTarget).attr("data-critsucc");
            const critfail = $(ev.currentTarget).attr("data-critfail");
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": "spell", "attack": false, "weaponid": item.id, "details":details,"critsucc":critsucc,"critfail":critfail};
            console.log(inputs);
            skillRoll(inputs);
        });
        html.find('.item-create').click(this._onItemCreate.bind(this));
        

        html.find('.add-condition').click(async ev => {
          let actorid = this.actor._id;
          let pack = await game.packs.get("flames.zweihanderconditions");
          let index = await pack.getIndex();
          const condition = document.getElementById(`${actorid}-picker`).value;
          const actor = this.actor;
          let finalItem = await pack.getEntry(condition);
          await actor.createEmbeddedEntity('OwnedItem', finalItem);
          console.log(`${actor.name} is gaining condition ${finalItem.name}`);
      });

        html.find('.roll-chaos-wounds').click( async ev => {
          let numdice = $(ev.currentTarget).attr("data-numdice");
          const tabletype = $(ev.currentTarget).attr("data-type");
          let output = await rollChaos(numdice);
          const tablename = tabletype + ` Injury Table`;
         
          var i;
          var counter = "Chaos Dice Were: ";
          for (i=0; i<output.result.length; i++){
            
            counter += output.result[i];
            if (i<(output.result.length -1)){counter += ", ";}
          }
          if (output.numbersuccess > 0){
            ChatMessage.create({ content: `<div>${counter}</div><div>You took an injury!</div>`, speaker: ChatMessage.getSpeaker({alias: this.actor.name})      });
          game.tables.getName(tablename).draw();}
          else{
            ChatMessage.create({ content: `<div>${counter}</div><div>You did not take an injury!</div>`, speaker: ChatMessage.getSpeaker({alias: this.actor.name})      });
          }
      });


      html.find('.generate-attributes').click(ev => {
        if (ev.shiftKey){
        generateAttributes(this.actor)}
        else (ui.notifications.info("Shift-Click to Regenerate Your Attributes"))
      });

      html.find('.roll-peril').click(ev => {
        rollPeril(this.actor);

      });



html.find('.item-delete').click(ev => {
    console.log(ev.currentTarget);
    let li = $(ev.currentTarget).parents(".item-name"),
    itemId = li.attr("data-item-id");
    console.log(li);
    console.log(`itemID =0 ${itemId}`);
    let item = this.actor.items.get(itemId);
    if (item.type == "Talent")
    {ui.notifications.info("Please Remove Talents using the Profession");
      return false;}
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);
  });

  html.find('.profession-delete').click(ev => {
    console.log(ev.currentTarget);
    let li = $(ev.currentTarget).parents(".item-name");
    const itemId = li.attr("data-item-id");
    const item = this.actor.items.get(itemId);
    let skillnumber = "skill0";
    var i;
    for(i = 1; i<11; i++){
      skillnumber = `skill${i}`;
      if (item.data.data[skillnumber].skillpurchased == true){
        ui.notifications.error("Skills Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<8; i++){
      skillnumber = `bonus${i}`;
      if (item.data.data[skillnumber].bonuspurchased == true){
        ui.notifications.error("Bonus Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<4; i++){
      skillnumber = `talent${i}`;
      if (item.data.data[skillnumber].talentpurchased == true){
        ui.notifications.error("Talent Remaining to Remove");
        return;
      }
    }
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);

  });

    html.find('.damage-roll').click(ev => {
      const li = $(ev.currentTarget).parents(".item-name");
      let itemID = li.data("itemId");
      console.log(itemID);
      const item = this.actor.data.items.get(itemID)
      let inputs = {"damagebonus": item.data.data.damagebonus, "damage": item.data.data.damage, "actor": this.actor};
      console.log(inputs)
      damageRoll(inputs);
  });

  html.find('.item-edit').click(ev => {
    const li = $(ev.currentTarget).parents(".item-name");
    let itemID = li.data("itemId");
    console.log(itemID);
    const item = this.actor.data.items.get(itemID)
    
      item.sheet.render(true);
  });


    html.find('.chaos-increase').click(ev => {
      const newvalue = $(ev.currentTarget).attr("data-ranks");
      let inputs = {"newvalue": newvalue, "actor": this.actor}
      chaosChange(inputs);
  });

  html.find('.chaos-reduce').click(ev => {
    const newvalue = $(ev.currentTarget).attr("data-ranks");
    let inputs = {"newvalue": newvalue, "actor": this.actor}
    chaosChange(inputs);
});

html.find('.order-increase').click(ev => {
  const newvalue = $(ev.currentTarget).attr("data-ranks");
  let inputs = {"newvalue": newvalue, "actor": this.actor}
  orderChange(inputs);
});

html.find('.order-reduce').click(ev => {
const newvalue = $(ev.currentTarget).attr("data-ranks");
let inputs = {"newvalue": newvalue, "actor": this.actor}
orderChange(inputs);
});

html.find(".show-hidden").click(event => this._showItemDescription(event));

html.find('.convert-gold').click(async ev => {
  this.actor.convertGold();

});

html.find('.convert-silver').click(async ev => {
  this.actor.convertSilver();

});

html.find('.convert-brass').click(async ev => {
  this.actor.convertBrass();

});

html.find('.trapping-spend').click(async ev => {
    let li = $(ev.currentTarget).parents(".item-name"),
    itemId = li.attr("data-item-id");
    const item = this.actor.items.get(itemId);
    if (item.data.data.quantity > 0){
      let newquantity = item.data.data.quantity -1;
      item.update({['data.quantity']: newquantity});
    }
    else{ui.notifications.error('No More to use')}

});

html.find('.reset-profession').click(ev => {
  const professions = this.actor.data.items.filter(function(item) {return item.type == "Profession"} );
  console.log("Profession Reset");
  var i;
  var skillnumber;
  for (const element of professions) {
    console.log(element.name);
    for(i=1; i< 11; i++){
      skillnumber = `skill${i}`;
      if (element.data.data[skillnumber].skillpurchased === true)
      {console.log(element.data.data[skillnumber].skillname);}  
    }
    for(i=1; i< 8; i++){
      skillnumber = `bonus${i}`;
      if (element.data.data[skillnumber].bonuspurchased === true)
      {console.log(element.data.data[skillnumber].bonusname);}  
    }
    for(i=1; i< 4; i++){
      skillnumber = `talent${i}`;
      if (element.data.data[skillnumber].talentpurchased === true)
      {console.log(element.data.data[skillnumber].talentname);}  
    }
  }
  
  
});

html.find('.trapping-add').click(async ev => {
    let li = $(ev.currentTarget).parents(".item-name"),
    itemId = li.attr("data-item-id");
    const item = this.actor.items.get(itemId);
    let newquantity = item.data.data.quantity + 1;
    item.update({['data.quantity']: newquantity});
    

});



html.find('.open-hidden-qualities').click(ev => {
  var x = document.getElementById("quality-section");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
});

    }

    _showItemDescription(event) {
      event.preventDefault();
  
      const toggler = $(event.currentTarget);
      const item = toggler.parents(".item-card");
      const description = item.find(".hidden-tab");
  
      $(description).slideToggle(function () {
        $(this).toggleClass("open");
      });
    }

    _onItemCreate(event) {
      event.preventDefault();
      let element = event.currentTarget;
      let itemData = {
        name: "Name",
        type: element.dataset.type
      };
      return this.actor.createOwnedItem(itemData);
    }

    
}
