import {skillRoll} from '/systems/flames/scripts/diceRoll.js';
import {rollChaos} from '../../scripts/rollChaos.js';
import {damageRoll} from '../../scripts/damageRoll.js'

export default class NPCSheet extends ActorSheet {
    get template() {
        return `systems/flames/templates/sheets/npc-sheet.hbs`;
    }


    getData() {
        const data = super.getData();
        const itemData = data.data;
        data.item = itemData;
        data.data = itemData.data;
        data.weapons = data.items.filter(function(item) {return item.type == "Weapon"} );
        data.conditions = data.items.filter(function(item) {return item.type == "Condition"} );
        data.gameType = game.settings.get('flames', 'gameName');
        data.skillNames = CONFIG.flamesSkills.skillnames;
        let groupName = "data.damage.value";
        let damagechoices = {0: "Unharmed", 1: "Lightly Wounded", 2: "<a class='roll-chaos-wounds' data-numdice='1' data-type='Moderate'> Moderately Wounded</a>", 3: "<a class='roll-chaos-wounds' data-numdice='2' data-type='Serious'>Seriously Wounded</a>", 4: "<a class='roll-chaos-wounds' data-numdice='3' data-type='Grievous'>Grievously Wounded</a>", 5: "Slain!"};
        data.damagechoices = damagechoices;
        let perilgroupName = "data.peril.value";
        let perilchoices = {0: "Unharmed", 1: "Imperiled", 2: "Ignore 1 Skill Rank", 3: "Ignore 2 Skill Ranks", 4: "Ignore 3 Skill Ranks", 5: "Incapacitated!"};
        data.perilchoices = perilchoices;
        console.log(data);
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html)

        html.find(".item-create").click(this._onItemCreate.bind(this));

            html.find('.skill-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const type = $(ev.currentTarget).attr("data-type");
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type};
            skillRoll(inputs);
        });

        html.find('.roll-chaos-wounds').click( async ev => {
            let numdice = $(ev.currentTarget).attr("data-numdice");
            const tabletype = $(ev.currentTarget).attr("data-type");
            let output = await rollChaos(numdice);
            const tablename = tabletype + ` Injury Table`;
            var i;
            var counter = "Chaos Dice Were: ";
            for (i=0; i<output.result.length; i++){
              
              counter += output.result[i];
              if (i<(output.result.length -1)){counter += ", ";}
            }
            if (output.numbersuccess > 0){
              ChatMessage.create({ content: `<div>${counter}</div><div>You took an injury!</div>`, speaker: ChatMessage.getSpeaker({alias: this.actor.name})      });
            game.tables.getName(tablename).draw();}
            else{
              ChatMessage.create({ content: `<div>${counter}</div><div>You did not take an injury!</div>`, speaker: ChatMessage.getSpeaker({alias: this.actor.name})      });
            }
        });

        html.find('.damage-roll').click(ev => {
            let li = $(ev.currentTarget).parents(".item-name");
            const itemId = li.attr("data-item-id");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            let inputs = {"damagebonus": item.data.data.damagebonus, "damage": item.data.data.damage, "actor": this.actor};
            damageRoll(inputs);
        });

        html.find('.attack-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            let type = "advanced";
            if (skill == "simplemelee" || skill == "simpleranged"){
              type = "basic";
            }
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type, "attack": true, "weaponid": item.id };
            console.log(inputs);
            skillRoll(inputs);
        });

        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        html.find('.item-delete').click(ev => {
          console.log(ev.currentTarget);
          let li = $(ev.currentTarget).parents(".item-name"),
          itemId = li.attr("data-item-id");
          console.log(li);
          console.log(`itemID =0 ${itemId}`);
          let item = this.actor.getOwnedItem(li.data("itemId"));
          if (item.type == "Talent")
          {ui.notifications.info("Please Remove Talents using the Profession");
            return false;}
          this.actor.deleteEmbeddedDocuments("Item", [itemId]);
        });


    }

    _onItemCreate(event) {
      event.preventDefault();
      let element = event.currentTarget;
      let itemData = {
        name: "Name",
        type: element.dataset.type
      };
      return this.actor.createOwnedItem(itemData);
    }
}